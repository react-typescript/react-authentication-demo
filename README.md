# React Authentication Demo 

* React Router 5.x 
* Simulate Login and Logout
* Save token to localstorage
* Create private routes
* Hide DOM elements if user is not logged 
* Create an HTTP interceptor hook

## Installation

```
npm install
npm start
```

## Run server

After installation, open a new terminal and run the server

```
npm run server
```

## Try the app

Visit [http://localhost:3000/](http://localhost:3000/) in your web browser and use the app
