import React from 'react';
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import { PageHome } from './features/PageHome';
import { PageAdmin } from './features/PageAdmin';
import { PageLogin } from './features/PageLogin';
import { PageSettings } from './features/PageSettings';
import { Navbar } from './core/components/Navbar';
import { PrivateRoute } from './core/auth/PrivateRoute';
import { useInterceptor } from './core/auth/useInterceptor';


const App: React.FC = () => {
  const {  error, request } = useInterceptor();
  console.log(request)
  return (
    <BrowserRouter>
      <div>

        {error && <div className="alert alert-danger">Server side error</div>}

        <Navbar />

        <Switch>
          <Route path="/home">
            <PageHome />
          </Route>
          <Route path="/login">
            <PageLogin />
          </Route>
          <PrivateRoute path="/settings">
            <PageSettings />
          </PrivateRoute>
          <PrivateRoute path="/admin">
            <PageAdmin />
          </PrivateRoute>
          <Route path="*">
            <Redirect to={{ pathname: "/login" }} />
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
};

export default App;
