import React, { useEffect, useState } from 'react';
import Axios from 'axios';

interface Product {
  id: number;
}

export const PageAdmin: React.FC = () => {
  const [products, setProducts] = useState<Product[]>([]);

  useEffect(() => {
    (async function () {
      Axios.get<Product[]>('http://localhost:3001/products')
        .then(result => setProducts(result.data))
    })()
  }, []);


  return <div>
    <h3>Admin Area</h3>

    Total: {products.length} products
  </div>
};
