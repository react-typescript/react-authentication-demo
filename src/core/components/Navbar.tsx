import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { useHistory } from 'react-router';
import cn from 'classnames';

import { signOut } from '../auth/authentication.service';
import { IfLogged } from '../auth/IfLogged';

export const Navbar: React.FC = () => {
  const history = useHistory();
  const {  pathname } = useLocation();

  const signOutHandler = () => {
      history.push("/login");
      signOut();
  };

  console.log('render navbar')

  return (
    <nav className="navbar navbar-expand navbar-light bg-light">
      <div className="navbar-brand">
        <Link to="/login">React Auth</Link>
      </div>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className={cn('nav-item', {'active': pathname === '/home'})}>
            <Link className="nav-link" to="/home">Home</Link>
          </li>
          <IfLogged>
            <li className={cn('nav-item', {'active': pathname === '/admin'})}>
              <Link className="nav-link" to="/admin">Admin</Link>
            </li>
            <li className={cn('nav-item', {'active': pathname === '/settings'})}>
              <Link className="nav-link" to="/settings">Settings</Link>
            </li>
          </IfLogged>
          <li className="nav-item" onClick={signOutHandler}>
            <div className="nav-link" >Logout</div>
          </li>
        </ul>
      </div>
    </nav>
  )
};
