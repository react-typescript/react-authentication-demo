import { useEffect, useState } from 'react';
import Axios from 'axios';
import { getItemFromLocalStorage } from './localstorage.helper';

export const useInterceptor = () => {
  const [request, setRequest] = useState<any>(null)
  const [error, setError] = useState<boolean>(false)

  useEffect(() => {
    Axios.interceptors.request.use( (config) => {
      const cfg = {
        ...config,
        headers: {
          ...config.headers,
          'Authentication-JWT': 'bearer ' + getItemFromLocalStorage('token')
        }
      };

      setError(false);
      setRequest(cfg)

      return cfg;
    });

    Axios.interceptors.response.use( (response) => {
      return response;
    }, function (error) {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      console.log('ERROR', error)
      setError(true);
      return Promise.reject(error);
    });

  },[])

  return {
    request,
    error
  };
}
