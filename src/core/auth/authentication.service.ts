import {
  getItemFromLocalStorage,
  removeItemLocalStorage,
  setItemLocalStorage
} from './localstorage.helper';
import Axios from 'axios';

export interface Credentials {
  username: string;
  password: string;
}

export interface Auth {
  accessToken: string;
}

export function signIn(url: string, params: Credentials): Promise<Auth> {

  const api = `${url}?username=${params.username}&password=${params.password}`;

  return Axios.get<Auth>(api)
    .then(res => {
      setItemLocalStorage('token', res.data.accessToken);
      return res.data
    })

}


export function signOut() {
  removeItemLocalStorage('token');
}

export function isLogged(): boolean {
  return !!getItemFromLocalStorage('token')
}
/*

// INTERCEPTORS
// add token to each request
Axios.interceptors.request.use( (config) => {
  console.log(config)
  return {
    ...config,
    headers: {
      ...config.headers,
      'Authentication-JWT': 'bearer ' + getItemFromLocalStorage('token')
    }
  };
});

// handle errors
Axios.interceptors.response.use( (response) => {
  console.log(response)
  return response;
}, function (error) {
  // Any status codes that falls outside the range of 2xx cause this function to trigger
  console.log('ERROR', error)
  return Promise.reject(error);
});
*/
